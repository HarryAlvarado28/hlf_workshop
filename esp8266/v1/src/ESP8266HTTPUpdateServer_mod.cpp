#include <Arduino.h>
#include <WiFiClient.h>
#include <WiFiServer.h>
#include <ESP8266WebServer.h>
#include <WiFiUdp.h>
#include "ESP8266HTTPUpdateServer_mod.h"
#include "Hash.h"
#include <ESP8266HTTPClient.h>
#include <ArduinoJson.h>


const char* ESP8266HTTPUpdateServer::_serverIndex =
R"(<html><body><form method='POST' action='' enctype='multipart/form-data'>
                  <input type='file' name='update'>
                  <input type='submit' value='Update'>
               </form>
         </body></html>)";
const char* ESP8266HTTPUpdateServer::_failedResponse = R"(Update Failed!)";
const char* ESP8266HTTPUpdateServer::_successResponse = "<META http-equiv=\"refresh\" content=\"15;URL=\">Update Success! Rebooting...";

int chunk_counter;
const char* token;
bool _verified = false;
unsigned char hash[20];
SHA1_CTX context;

ESP8266HTTPUpdateServer::ESP8266HTTPUpdateServer(bool serial_debug)
{
  _serial_output = serial_debug;
  _server = NULL;
  _username = NULL;
  _password = NULL;
  _authenticated = false;
  SHA1Init(&context);
}

void ESP8266HTTPUpdateServer::setup(ESP8266WebServer *server, const char * path, const char * username, const char * password)
{
    _server = server;
    _username = (char *)username;
    _password = (char *)password;

    // handler for the /update form page
    _server->on(path, HTTP_GET, [&](){
      if(_username != NULL && _password != NULL && !_server->authenticate(_username, _password))
        return _server->requestAuthentication();
      _server->send(200, "text/html", _serverIndex);
    });

    // handler for the /update form POST (once file upload finishes)
    _server->on(path, HTTP_POST, [&](){
      if(!_authenticated)
        return _server->requestAuthentication();
      _server->send(200, "text/html", Update.hasError() ? _failedResponse : _successResponse);
      ESP.restart();
    },[&](){
      // handler for the file upload, get's the sketch bytes, and writes
      // them through the Update object
      HTTPUpload& upload = _server->upload();
      if(upload.status == UPLOAD_FILE_START){
        if (_serial_output)
          Serial.setDebugOutput(true);

        _authenticated = (_username == NULL || _password == NULL || _server->authenticate(_username, _password));
        if(!_authenticated){
          if (_serial_output)
            Serial.printf("Unauthenticated Update\n");
          return;
        }

        WiFiUDP::stopAll();
        
        if (_serial_output) {
          Serial.printf("Current fimware MD5: %s\n", ESP.getSketchMD5().c_str());
          Serial.printf("Update: %s\n", upload.filename.c_str());          
		    }
		
		    chunk_counter = 0;
        
        uint32_t maxSketchSpace = (ESP.getFreeSketchSpace() - 0x1000) & 0xFFFFF000;
        if(!Update.begin(maxSketchSpace)){//start with max available size
          if (_serial_output) Update.printError(Serial);
        }
      } else if(_authenticated && upload.status == UPLOAD_FILE_WRITE){
        if (_serial_output) {
          //Serial.printf(".");
          const char * current_hash = sha1(upload.buf, upload.currentSize).c_str();
          chunk_counter++;
          Serial.printf("Chunk %d size: %d ", chunk_counter, upload.currentSize);
          Serial.printf("Hash: %s\n", current_hash);

          if (upload.currentSize < 2048) {
            sha1_firmware(upload.buf, upload.currentSize, hash, context, true);
            unsigned char human_readable_hash[40];
            int i = 0;
            for (i=0; i < 20; i++) {
              sprintf((char*)&(human_readable_hash[i*2]), "%02x", hash[i]);
            }
            Serial.printf("final hash is %s\n", human_readable_hash);

            _verified = verified((const char *)human_readable_hash);
            if (!_verified) {
              Serial.printf("Verification FAILED! OTA update aborted! Will REBOOT!\n\n");
              ESP.restart();
            } 
          }
          sha1_firmware(upload.buf, upload.currentSize, hash, context, false);
        }
        if(Update.write(upload.buf, upload.currentSize) != upload.currentSize){
          if (_serial_output) Update.printError(Serial);

        }
      } else if(_authenticated && upload.status == UPLOAD_FILE_END){
        if(Update.end(true)){ //true to set the size to the current progress
          if (_serial_output) {
            Serial.printf("Update Success: %u\nRebooting...\n", upload.totalSize);
		      }
        } else {
          if (_serial_output) Update.printError(Serial);
        }
        if (_serial_output) Serial.setDebugOutput(false);
      } else if(_authenticated && upload.status == UPLOAD_FILE_ABORTED){
        Update.end();
        if (_serial_output) Serial.println("Update was aborted");
      }
      delay(0);
    });
}

bool ESP8266HTTPUpdateServer::verified(const char * hash) 
{
	Serial.println("Verifying with blockchain...");
			  
	HTTPClient http;

	http.begin("http://192.168.1.20:4000/users");
	http.addHeader("content-type", "application/x-www-form-urlencoded");

	int httpCode = http.POST("username=ESP8266&orgName=org1");
	String payload = http.getString();

	Serial.println(httpCode);
	Serial.println(payload);

	if (httpCode == 200) {
		const size_t bufferSize = JSON_OBJECT_SIZE(2) + JSON_OBJECT_SIZE(3) + JSON_OBJECT_SIZE(5) + JSON_OBJECT_SIZE(8) + 370;
		DynamicJsonBuffer jsonBuffer(bufferSize);
		JsonObject& root = jsonBuffer.parseObject(payload);
		token = root["token"];
		Serial.print("Hyperledger Client Token: ");
		Serial.println(token);

		http.begin("http://192.168.1.20:4000/channels/mychannel/chaincodes/mycc");

		char auth[1000];
    sprintf(auth, "Bearer %s", token);
		http.addHeader("authorization", auth);
		http.addHeader("content-type", "application/json");

    char request[1000];
    sprintf(request, "{\"fcn\": \"verify\", \"args\": [\"ESP00001\", \"%s\"]}", hash);
    Serial.println(request);
    int httpCode = http.POST(request);
		String payload = http.getString();

		Serial.println(httpCode);
		Serial.printf("Transaction ID: %s\n", payload.c_str());
		
		if (payload.length() == 64) {
      delay(1000);

      const char * transaction_id = "ESP00001";

      String url_prefix = "http://192.168.1.20:4000/channels/mychannel/chaincodes/mycc?peer=peer1&fcn=query_status&args=%5B%22";
      String url_suffix = "%22%5D";
      String url = url_prefix + transaction_id + url_suffix;

      http.begin(url);
      http.addHeader("authorization", auth);
		  http.addHeader("content-type", "application/json");

      httpCode = http.GET();
      payload = http.getString();

      Serial.println(payload);

      JsonObject& resp_root = jsonBuffer.parseObject(payload);
		  const char * status = resp_root["status"];
      const char * failed_attemps = resp_root["failed_attemps"];

      if (strcmp(status,"verified") == 0) {
        Serial.println("Verification Succeed! Firmware is legtimate, will reboot to finalize the OTA update.");
        return true;
      } else {
        Serial.printf("Verification FAILED. Failed attempts made: %s\n", failed_attemps);
      }
		}
	}

	http.end();
	return false;
}
